class EventsController < ApplicationController
  def index
    respond_to do |format|
      format.json { render json: { events: events.as_json } }
      format.html {  }
    end
  end

  private

  def events
    return Event.all unless filter_condition_params
    Events::Filter.call(filter_condition: filter_condition)
  end

  def filter_condition
    FilterCondition.new(filter_condition_params)
  end

  def filter_condition_params
    return nil unless params[:filter_condition]
    params.require(:filter_condition).permit(
      :city,
      :min_start_date,
      :max_start_date,
      :min_start_time,
      :max_start_time,
      :topic_name,
    )
  end
end
