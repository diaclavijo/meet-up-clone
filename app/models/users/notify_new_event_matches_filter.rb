module Users
  class NotifyNewEventMatchesFilter
    def self.call(event:)
      FilterConditions::QueryFilterConditionsForEvent.call(event: event)
        .includes(:user)
        .each do |filter_condition|
        user = filter_condition.user
        browser_push_notification = BrowserPushNotifications::FilterConditionMatchesEvent.new(
          user: user,
          filter_condition: filter_condition,
        )
        browser_push_notification.deliver
        DeliverNewEventMatchesFilterIfNeededJob.set(
          wait_until: 5.minutes.from_now,
        ).perform_later(
          user_id: user,
          filter_condition_id: filter_condition,
          event_id: event,
          sent_notification_id: browser_push_notification.sent_notification.id,
        )
      end
    end
  end
end

