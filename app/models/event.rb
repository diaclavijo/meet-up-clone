class Event < ApplicationRecord
  has_many :events_topic
  has_many :topics, through: :events_topic
end
