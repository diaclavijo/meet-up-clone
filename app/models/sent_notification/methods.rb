class SentNotification
  class Methods
    class << self
      def email
        'email'
      end

      def browser_push_notification
        'browser_push_notification'
      end
    end
  end
end

