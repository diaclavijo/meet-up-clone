module FilterConditions
  # Given a filter condition, find all events that satisfy the condition
  class QueryFilterConditionsForEvent
    def self.call(event:)
      FilterCondition.where('city = ? OR city IS NULL', event.city)
        .where('topic_name IS NULL OR topic_name IN (?)', event.topics.map(&:name).map(&:downcase))
        .where('min_start_time IS NULL OR ?::time >= min_start_time::time', event.start_at)
        .where('max_start_time IS NULL OR ?::time <= max_start_time::time', event.start_at)
        .where('min_start_date IS NULL OR ?::date >= min_start_date::date', event.start_at)
        .where('max_start_date IS NULL OR ?::date <= max_start_date::date', event.start_at)
    end
  end
end


