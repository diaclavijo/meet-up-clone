module BrowserPushNotifications
  class FilterConditionMatchesEvent < Base
    def title
      'You got a new event matching your conditions'
    end

    def body
      'Just that, and sorry about the design, we are working on it!'
    end

    def key
      SentNotification::Keys.filter_condition_matches_event
    end
  end
end

