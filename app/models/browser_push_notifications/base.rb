module BrowserPushNotifications
  class Base
    attr_reader :user, :filter_condition, :sent_notification

    def initialize(user:, filter_condition:)
      @user = user
      @filter_condition = filter_condition
    end

    def deliver
      Services::PushNotificationService.new.deliver(
        user: user,
        title: title,
        body: body,
      )
      @sent_notification = SentNotification.create(
        key: key,
        method: SentNotification::Methods.browser_push_notification,
        user: user,
        title: title,
        body: body,
      )
    end

    def key
      raise NotImplementedError
    end

    def title
      raise NotImplementedError
    end

    def body
      raise NotImplementedError
    end
  end
end

