module Events
  class Create
    def self.call(event:)
      event.create!
      Users::NotifyNewEventMatchesFilterJob(event_id: event.id).perform
    end
  end
end

