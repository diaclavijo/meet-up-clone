module Events
  class Filter
    # Given a filter condition, it returns all the events that satisfy the conditions
    def self.call(filter_condition:)
      events = Event.all
      events = events.joins(events_topic: :topic).where(topics: { name: filter_condition.topic_name.downcase }) if filter_condition.topic_name
      events = events.where(city: filter_condition.city) if filter_condition.city
      events = events.where("start_at::time >= ?::time", filter_condition.min_start_time) if filter_condition.min_start_time
      events = events.where("start_at::time <= ?::time", filter_condition.max_start_time) if filter_condition.max_start_time
      events = events.where("start_at::date >= ?::date", filter_condition.min_start_date) if filter_condition.min_start_date
      events = events.where("start_at::date <= ?::date", filter_condition.max_start_date) if filter_condition.max_start_date
      events
    end
  end
end
