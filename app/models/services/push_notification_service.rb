module Services
  class PushNotificationService

    def deliver(args)
      Rails.logger.info "Delivering push notification with args: #{args}"
    end
  end
end

