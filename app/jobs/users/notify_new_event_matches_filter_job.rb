module Users
  class NotifyNewEventMatchesFilterJob < ::ApplicationJob
    def perform(event_id:)
      event = Event.find(event_id)
      Users::NotifyNewEventMatchesFilter.call(event: event)
    end
  end
end

