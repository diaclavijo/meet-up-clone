class DeliverNewEventMatchesFilterIfNeededJob < ActiveJob::Base
  def perform(sent_notification_id:, user_id:, filter_condition_id:, event_id:)
    sent_notification = SentNotification.find(sent_notification_id)
    return if sent_notification.read

    user = User.find(user_id)
    filter_condition = FilterCondition.find(filter_condition_id)
    event = Event.find(event_id)
    FilterConditionMatchesEventMailer.basic(
      user: user,
      filter_condition: filter_condition,
      event: event,
    ).deliver
    SentNotification.create(
      key: SentNotification::Keys.filter_condition_matches_event,
      method: SentNotification::Methods.email,
      user: user,
    )
  end
end
