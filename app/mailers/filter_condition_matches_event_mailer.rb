class FilterConditionMatchesEventMailer < ApplicationMailer
  def basic(user:, filter_condition:, event:)
    @user = user
    @filter_condition = filter_condition
    @event = event
    mail(to: user.email)
  end
end
