require 'test_helper'

class Events::FilterTest < ActiveSupport::TestCase
  test "returns ruby event when search is Ruby" do
    filter_condition = FilterCondition.new(topic_name: 'rails')
    assert_equal Events::Filter.call(filter_condition: filter_condition), [events(:rails)]
  end

  test "returns right events when min_start_time is 13:00" do
    filter_condition = FilterCondition.new(min_start_time: '13:00')
    assert_equal Events::Filter.call(filter_condition: filter_condition), [events(:ruby)]
  end

  test "returns right events when min_start_time 08:00" do
    filter_condition = FilterCondition.new(min_start_time: '08:00')
    assert_equal Events::Filter.call(filter_condition: filter_condition), [events(:rails), events(:ruby)]
  end

  test "returns right events when min_start_time is 20:00" do
    filter_condition = FilterCondition.new(min_start_time: '20:00')
    assert_equal Events::Filter.call(filter_condition: filter_condition), []
  end

  test "returns right events when max_start_time is 13:00" do
    filter_condition = FilterCondition.new(max_start_time: '13:00')
    assert_equal Events::Filter.call(filter_condition: filter_condition), [events(:rails)]
  end

  test "returns right events when min_start_date is 1/12/17" do
    filter_condition = FilterCondition.new(min_start_date: Date.new(2017, 12, 1))
    assert_equal Events::Filter.call(filter_condition: filter_condition), [events(:rails), events(:ruby)]
  end

  test "returns right events when min_start_date is 16/12/17" do
    filter_condition = FilterCondition.new(min_start_date: Date.new(2017, 12, 16))
    assert_equal Events::Filter.call(filter_condition: filter_condition), [events(:ruby)]
  end

  test "returns right events when max_start_date is 15/12/17" do
    filter_condition = FilterCondition.new(max_start_date: Date.new(2017, 12, 15))
    assert_equal Events::Filter.call(filter_condition: filter_condition), [events(:rails)]
  end

  test "returns ruby when city is Las Palmas" do
    filter_condition = FilterCondition.new(city: 'Las Palmas')
    assert_equal Events::Filter.call(filter_condition: filter_condition), [events(:rails)]
  end

end
