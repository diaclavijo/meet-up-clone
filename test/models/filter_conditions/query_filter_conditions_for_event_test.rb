require 'test_helper'

class FilterConditions::QueryFilterConditionsForEventTest < ActiveSupport::TestCase
  test "when event is in location Las Palmas, filters for Tenerife are excluded" do
    event = Event.new(city: 'Las Palmas')
    refute_includes FilterConditions::QueryFilterConditionsForEvent.call(event: event),
      filter_conditions(:location_tenerife)
  end

  test "when event starts at 08:00, filters with min_start_time at 09:00 are excluded" do
    event = Event.new(start_at: Time.zone.parse('08:00'))
    refute_includes FilterConditions::QueryFilterConditionsForEvent.call(event: event),
      filter_conditions(:start_time_after_9)
  end

  test "when event starts at 15:00, filters with max_start_time at 14:00 are excluded" do
    event = Event.new(start_at: Time.zone.parse('15:00'))
    refute_includes FilterConditions::QueryFilterConditionsForEvent.call(event: event),
      filter_conditions(:start_time_before_14)
  end

  test "when event starts on 1/12/17, filters with min_start_date on 2/12/17 are excluded" do
    event = Event.new(start_at: Time.zone.parse('2017-12-01'))
    refute_includes FilterConditions::QueryFilterConditionsForEvent.call(event: event),
      filter_conditions(:min_start_date_2017_12_2)
  end

  test "when event starts on 16/12/17, filters with max_start_date on 15/12/17 are excluded" do
    event = Event.new(start_at: Time.zone.parse('2017-12-16'))
    refute_includes FilterConditions::QueryFilterConditionsForEvent.call(event: event),
      filter_conditions(:max_start_date_2017_12_15)
  end

  test "when event is about topic Ruby, filters with other topics are excluded" do
    refute_includes FilterConditions::QueryFilterConditionsForEvent.call(event: events(:ruby)),
      filter_conditions(:topic_rails)
  end

  test "when event is Rails, filters chosen are right" do
    assert_equal FilterConditions::QueryFilterConditionsForEvent.call(event: events(:rails)).sort,
      [
        filter_conditions(:topic_rails),
        filter_conditions(:location_las_palmas),
        filter_conditions(:start_time_after_9),
        filter_conditions(:start_time_before_14),
        filter_conditions(:max_start_date_2017_12_15),
        filter_conditions(:empty),
      ].sort
  end
end
