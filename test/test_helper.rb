require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  set_fixture_class 'events/filter_conditions' => Events::FilterCondition
  # set_fixture_class 'events_topic' => EventsTopic

  # Add more helper methods to be used by all tests here...
end
