require 'test_helper'

class EventsControllerTest < ActionDispatch::IntegrationTest
  test 'GET /events' do
    get events_path, as: :json
    assert_response :success
    assert_equal({ 'events' => Event.all.as_json }.to_json, response.body)
  end

  test 'GET /events?filter_condition:{city=LasPalmas&min_start_date=2017-12-1}' do
    params = {
      filter_condition: {
        city: 'Las Palmas',
        min_start_date: '2017-12-1',
      },
    }
    get events_path(params), as: :json
    assert_response :success
    assert_equal({ 'events' => [events(:rails)] }.to_json, response.body)
  end
end

