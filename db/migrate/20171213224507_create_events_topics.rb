class CreateEventsTopics < ActiveRecord::Migration[5.1]
  def change
    create_table :events_topics do |t|
      t.references :event
      t.references :topic

      t.timestamps
    end
  end
end
