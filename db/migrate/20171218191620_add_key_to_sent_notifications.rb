class AddKeyToSentNotifications < ActiveRecord::Migration[5.1]
  def change
    add_column :sent_notifications, :key, :string
  end
end
