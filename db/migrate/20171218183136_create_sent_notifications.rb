class CreateSentNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :sent_notifications do |t|
      t.boolean :read, default: false
      t.references :user
      t.string :method
      t.text :body
      t.string :title

      t.timestamps
    end
  end
end
