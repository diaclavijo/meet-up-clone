class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :city
      t.string :title
      t.text :description
      t.timestamp :start_at
      t.timestamp :end_at

      t.timestamps
    end
  end
end
