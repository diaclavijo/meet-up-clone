class CreateFilterConditions < ActiveRecord::Migration[5.1]
  def change
    create_table :filter_conditions do |t|
      t.string :city
      t.date :min_start_date
      t.date :max_start_date
      t.time :min_start_time
      t.time :max_start_time
      t.string :topic_name

      t.timestamps
    end
  end
end
